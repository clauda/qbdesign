var context = { courses: [
    {university: "Estácio", name: "Direito", discount: "40%" },
    {university: "Mackenzie", name: "Admistração", discount: "10%" },
    {university: "IBTA", name: "Análise e Desenvolvimento de Sistemas", discount: "25%" },
    {university: "UNICSUL", name: "Publicidade e Propaganda", discount: "30%" },
    {university: "UNISUAM", name: "Ciências Contábeis", discount: "30%" },
    {university: "Mackenzie", name: "Publicidade e Propaganda", discount: "30%" },
    {university: "Estácio", name: "Sistemas para Internet", discount: "40%" },
    {university: "IBTA", name: "Análise e Desenvolvimento de Sistemas", discount: "25%" },
    {university: "UNISUAM", name: "Ciências Contábeis", discount: "30%" }
  ]};

module.exports = {

  index: function(request, response){
    response.render('index', {
      title: 'Sample App',
      context: context.courses
    });
  }

};
