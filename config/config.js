var path = require('path')
  , rootPath = path.normalize(__dirname + '/..')
  , env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    host: 'localhost',
    app: {
      name: 'app'
    },
    port: 4000,
  },

  test: {
    root: rootPath,
    host: 'localhost',
    app: {
      name: 'app'
    },
    port: 4000,
  },

  production: {
    root: rootPath,
    app: {
      name: 'app'
    },
    port: process.env.PORT,
  }
};

module.exports = config[env];
