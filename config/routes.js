var statics = require('../app/controllers/statics');

module.exports = function(app){
  app.get('/', statics.index);
};
