QB = {}

# $('.carousel').carousel
#   interval: 5000

$('.tooltip-social').tooltip
  selector: "a[data-toggle=tooltip]"

$("#ranger").slider
  max: 10
  range: true
  values: [ 0, 5 ]

$("#ranger").slider("float", { rest: false })

$(".dial").knob()

$size = $(window).width()

$(window).on "resize", ->
  if ($size < 980)
    $(".filter").removeClass("fix-top")
    $("#tmp").remove()

$filter = $(".filter")

if ($filter.length > 0)
  top = $filter.offset().top

  $(window).on "scroll", ->
    if ($size > 980)
      r = if (window.pageYOffset) then window.pageYOffset else (document.documentElement || document.body.parentNode || document.body).scrollTop

      if (r > top)
        $filter.addClass("fix-top")
        if (!$("#tmp").length)
          $("<div id='tmp'></div>").height($(".filter").outerHeight(true)).insertAfter(".filter")
      else
        $filter.removeClass("fix-top")
        $("#tmp").remove()

