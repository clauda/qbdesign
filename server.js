var express = require('express')
  , fs = require('fs')
  , config = require('./config/config');

var app = express();

require('./app/helpers')(config);
require('./config/express')(app, config);
require('./config/routes')(app);

app.listen(config.port, config.host, function(){ console.log('Paaaarla!'); });

exports = module.exports = app;
